import ballerina/grpc;
import ballerina/protobuf;

//Define the protobuf descriptor for the Library Service
public const string LS_DESC = "0A084C532E70726F746F12076C696272617279229E010A0E416464426F6F6B5265717565737412140A057469746C6518012001280952057469746C6512160A06617574686F721802200128095206617574686F72121A0A086C6F636174696F6E18032001280952086C6F636174696F6E12120A046973626E18042001280952046973626E12160A06737461747573180520012808520673746174757312160A06757365724944180620012809520675736572494422250A0F416464426F6F6B526573706F6E736512120A046973626E18012001280952046973626E22480A11437265617465557365725265717565737412160A067573657249441801200128095206757365724944121B0A09757365725F7479706518022001280952087573657254797065222E0A1243726561746555736572526573706F6E736512180A076D65737361676518012001280952076D65737361676522A1010A11557064617465426F6F6B5265717565737412140A057469746C6518012001280952057469746C6512160A06617574686F721802200128095206617574686F72121A0A086C6F636174696F6E18032001280952086C6F636174696F6E12120A046973626E18042001280952046973626E12160A06737461747573180520012808520673746174757312160A067573657249441806200128095206757365724944222E0A12557064617465426F6F6B526573706F6E736512180A076D65737361676518012001280952076D657373616765223F0A1152656D6F7665426F6F6B5265717565737412120A046973626E18012001280952046973626E12160A06757365724944180220012809520675736572494422390A1252656D6F7665426F6F6B526573706F6E736512230A05626F6F6B7318012003280B320D2E6C6962726172792E426F6F6B5205626F6F6B73221B0A194C697374417661696C61626C65426F6F6B735265717565737422410A1A4C697374417661696C61626C65426F6F6B73526573706F6E736512230A05626F6F6B7318012003280B320D2E6C6962726172792E426F6F6B5205626F6F6B7322270A114C6F63617465426F6F6B5265717565737412120A046973626E18012001280952046973626E224E0A124C6F63617465426F6F6B526573706F6E7365121A0A086C6F636174696F6E18012001280952086C6F636174696F6E121C0A09617661696C61626C651802200128085209617661696C61626C6522400A11426F72726F77426F6F6B5265717565737412170A07757365725F6964180120012809520675736572496412120A046973626E18022001280952046973626E222E0A12426F72726F77426F6F6B526573706F6E736512180A076D65737361676518012001280952076D657373616765227F0A04426F6F6B12140A057469746C6518012001280952057469746C6512190A08617574686F725F311802200128095207617574686F7231121A0A086C6F636174696F6E18042001280952086C6F636174696F6E12120A046973626E18052001280952046973626E12160A067374617475731806200128085206737461747573329F040A0E4C69627261727953657276696365123E0A07416464426F6F6B12172E6C6962726172792E416464426F6F6B526571756573741A182E6C6962726172792E416464426F6F6B526573706F6E7365220012480A0B4372656174655573657273121A2E6C6962726172792E43726561746555736572526571756573741A1B2E6C6962726172792E43726561746555736572526573706F6E7365220012470A0A557064617465426F6F6B121A2E6C6962726172792E557064617465426F6F6B526571756573741A1B2E6C6962726172792E557064617465426F6F6B526573706F6E7365220012470A0A52656D6F7665426F6F6B121A2E6C6962726172792E52656D6F7665426F6F6B526571756573741A1B2E6C6962726172792E52656D6F7665426F6F6B526573706F6E73652200125F0A124C697374417661696C61626C65426F6F6B7312222E6C6962726172792E4C697374417661696C61626C65426F6F6B73526571756573741A232E6C6962726172792E4C697374417661696C61626C65426F6F6B73526573706F6E7365220012470A0A4C6F63617465426F6F6B121A2E6C6962726172792E4C6F63617465426F6F6B526571756573741A1B2E6C6962726172792E4C6F63617465426F6F6B526573706F6E7365220012470A0A426F72726F77426F6F6B121A2E6C6962726172792E426F72726F77426F6F6B526571756573741A1B2E6C6962726172792E426F72726F77426F6F6B526573706F6E73652200620670726F746F33";

// Define a client class for interacting with the Library Service
public isolated client class LibraryServiceClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

// Initialize the client with a gRPC endpoint URL and configuration
    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, LS_DESC);
    }
// Remote function to add a book
    isolated remote function AddBook(AddBookRequest|ContextAddBookRequest req) returns AddBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        AddBookRequest message;

        // Check if the request is a ContextAddBookRequest
        if req is ContextAddBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to add a book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/AddBook", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        // Return the result as an AddBookResponse
        return <AddBookResponse>result;
    }

// Remote function to add a book with context
    isolated remote function AddBookContext(AddBookRequest|ContextAddBookRequest req) returns ContextAddBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        AddBookRequest message;

        // Check if the request is a ContextAddBookRequest
        if req is ContextAddBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to add a book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/AddBook", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        // Return the result as a ContextAddBookResponse with headers
        return {content: <AddBookResponse>result, headers: respHeaders};
    }

// Remote function to create users
    isolated remote function CreateUsers(CreateUserRequest|ContextCreateUserRequest req) returns CreateUserResponse|grpc:Error {
        map<string|string[]> headers = {};
        CreateUserRequest message;
        // Check if the request is a ContextCreateUserRequest
        if req is ContextCreateUserRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to create users
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/CreateUsers", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        // Return the result as a CreateUserResponse
        return <CreateUserResponse>result;
    }
// Remote function to create users with context
    isolated remote function CreateUsersContext(CreateUserRequest|ContextCreateUserRequest req) returns ContextCreateUserResponse|grpc:Error {
        map<string|string[]> headers = {};
        CreateUserRequest message;
        // Check if the request is a ContextCreateUserRequest
        if req is ContextCreateUserRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to create users
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/CreateUsers", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        // Return the result as a CreateUserResponse
        return {content: <CreateUserResponse>result, headers: respHeaders};
    }
// Remote function to update a book
    isolated remote function UpdateBook(UpdateBookRequest|ContextUpdateBookRequest req) returns UpdateBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        UpdateBookRequest message;
        // Check if the request is a ContextUpdateBookRequest
        if req is ContextUpdateBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to update book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/UpdateBook", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        // Return the result as a UpdateBookResponse
        return <UpdateBookResponse>result;
    }
// Remote function to update a book with context
    isolated remote function UpdateBookContext(UpdateBookRequest|ContextUpdateBookRequest req) returns ContextUpdateBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        UpdateBookRequest message;
        // Check if the request is a ContextUpdateBookRequest
        if req is ContextUpdateBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
         // Execute the gRPC RPC call to update book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/UpdateBook", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        // Return the result as a UpdateBookResponse
        return {content: <UpdateBookResponse>result, headers: respHeaders};
    }
// Remote function to remove a book
    isolated remote function RemoveBook(RemoveBookRequest|ContextRemoveBookRequest req) returns RemoveBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        RemoveBookRequest message;
        // Check if the request is a ContextRemoveBookRequest
        if req is ContextRemoveBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to remove book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/RemoveBook", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
         // Return the result as a RemoveBookResponse
        return <RemoveBookResponse>result;
    }
// Remote function to remove a book with context
    isolated remote function RemoveBookContext(RemoveBookRequest|ContextRemoveBookRequest req) returns ContextRemoveBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        RemoveBookRequest message;
        // Check if the request is a ContextRemoveBookRequest
        if req is ContextRemoveBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to remove book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/RemoveBook", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        // Return the result as a RemoveBookResponse
        return {content: <RemoveBookResponse>result, headers: respHeaders};
    }
// Remote function to list available books
    isolated remote function ListAvailableBooks(ListAvailableBooksRequest|ContextListAvailableBooksRequest req) returns ListAvailableBooksResponse|grpc:Error {
        map<string|string[]> headers = {};
        ListAvailableBooksRequest message;
        // Check if the request is a ContextListAvailableBooksRequest
        if req is ContextListAvailableBooksRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to list available books
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/ListAvailableBooks", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        // Return the result as a ListAvailableBooksResponse
        return <ListAvailableBooksResponse>result;
    }
// Remote function to list available books with context
    isolated remote function ListAvailableBooksContext(ListAvailableBooksRequest|ContextListAvailableBooksRequest req) returns ContextListAvailableBooksResponse|grpc:Error {
        map<string|string[]> headers = {};
        ListAvailableBooksRequest message;
        // Check if the request is a ContextListAvailableBooksRequest
        if req is ContextListAvailableBooksRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to list available books
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/ListAvailableBooks", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        // Return the result as a ListAvailableBooksResponse
        return {content: <ListAvailableBooksResponse>result, headers: respHeaders};
    }
// Remote function to locate a book
    isolated remote function LocateBook(LocateBookRequest|ContextLocateBookRequest req) returns LocateBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        LocateBookRequest message;
        // Check if the request is a ContextLocateBookRequest
        if req is ContextLocateBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to locate book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/LocateBook", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        // Return the result as a LocateBookResponse
        return <LocateBookResponse>result;
    }
// Remote function to locate a book with context
    isolated remote function LocateBookContext(LocateBookRequest|ContextLocateBookRequest req) returns ContextLocateBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        LocateBookRequest message;
        // Check if the request is a ContextLocateBookRequest
        if req is ContextLocateBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
         // Execute the gRPC RPC call to locate book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/LocateBook", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
         // Return the result as a LocateBookResponse
        return {content: <LocateBookResponse>result, headers: respHeaders};
    }
// Remote function to borrow a book
    isolated remote function BorrowBook(BorrowBookRequest|ContextBorrowBookRequest req) returns BorrowBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        BorrowBookRequest message;
         // Check if the request is a ContextBorrowBookRequest
        if req is ContextBorrowBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        // Execute the gRPC RPC call to borrow book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/BorrowBook", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        // Return the result as a BorrowBookResponse
        return <BorrowBookResponse>result;
    }
// Remote function to borrow a book with context
    isolated remote function BorrowBookContext(BorrowBookRequest|ContextBorrowBookRequest req) returns ContextBorrowBookResponse|grpc:Error {
        map<string|string[]> headers = {};
        BorrowBookRequest message;
        // Check if the request is a ContextBorrowBookRequest
        if req is ContextBorrowBookRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
         // Execute the gRPC RPC call to borrow book
        var payload = check self.grpcClient->executeSimpleRPC("library.LibraryService/BorrowBook", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        // Return the result as a BorrowBookResponse
        return {content: <BorrowBookResponse>result, headers: respHeaders};
    }
}
// ContextRemoveBookRequest record type
public type ContextRemoveBookRequest record {|
    RemoveBookRequest content;
    map<string|string[]> headers;
|};
// ContextBorrowBookResponse record type
public type ContextBorrowBookResponse record {|
    BorrowBookResponse content;
    map<string|string[]> headers;
|};
// ContextListAvailableBooksRequest record type
public type ContextListAvailableBooksRequest record {|
    ListAvailableBooksRequest content;
    map<string|string[]> headers;
|};
// ContextAddBookRequest record type
public type ContextAddBookRequest record {|
    AddBookRequest content;
    map<string|string[]> headers;
|};
// ContextAddBookResponse record type
public type ContextAddBookResponse record {|
    AddBookResponse content;
    map<string|string[]> headers;
|};
// ContextRemoveBookResponse record type
public type ContextRemoveBookResponse record {|
    RemoveBookResponse content;
    map<string|string[]> headers;
|};
// ContextLocateBookRequest record type
public type ContextLocateBookRequest record {|
    LocateBookRequest content;
    map<string|string[]> headers;
|};
// ContextLocateBookResponse record type
public type ContextLocateBookResponse record {|
    LocateBookResponse content;
    map<string|string[]> headers;
|};
// ContextListAvailableBooksResponse record type
public type ContextListAvailableBooksResponse record {|
    ListAvailableBooksResponse content;
    map<string|string[]> headers;
|};
// ContextUpdateBookRequest record type
public type ContextUpdateBookRequest record {|
    UpdateBookRequest content;
    map<string|string[]> headers;
|};
// ContextBorrowBookRequest record type
public type ContextBorrowBookRequest record {|
    BorrowBookRequest content;
    map<string|string[]> headers;
|};
// ContextCreateUserResponse record type
public type ContextCreateUserResponse record {|
    CreateUserResponse content;
    map<string|string[]> headers;
|};
// ContextUpdateBookResponse record type
public type ContextUpdateBookResponse record {|
    UpdateBookResponse content;
    map<string|string[]> headers;
|};
// ContextCreateUserRequest record type
public type ContextCreateUserRequest record {|
    CreateUserRequest content;
    map<string|string[]> headers;
|};

// Descriptor for the RemoveBookRequest record type
@protobuf:Descriptor {value: LS_DESC}
public type RemoveBookRequest record {|
    string isbn = "";
    string userID = "";
|};
// Descriptor for the BorrowBookResponse record type
@protobuf:Descriptor {value: LS_DESC}
public type BorrowBookResponse record {|
    string message = "";
|};

// Descriptor for the ListAvailableBooksRequest record type
@protobuf:Descriptor {value: LS_DESC}
public type ListAvailableBooksRequest record {|
|};
// Descriptor for the AddBookRequest record type
@protobuf:Descriptor {value: LS_DESC}
public type AddBookRequest record {|
    string title = "";
    string author = "";
    string location = "";
    string isbn = "";
    boolean status = false;
    string userID = "";
|};
// Descriptor for the AddBookResponse record type
@protobuf:Descriptor {value: LS_DESC}
public type AddBookResponse record {|
    string isbn = "";
|};
// Descriptor for the RemoveBookResponse record type
@protobuf:Descriptor {value: LS_DESC}
public type RemoveBookResponse record {|
    Book[] books = [];
|};
// Descriptor for the LocateBookRequest record type
@protobuf:Descriptor {value: LS_DESC}
public type LocateBookRequest record {|
    string isbn = "";
|};
// Descriptor for the LocateBookResponse record type
@protobuf:Descriptor {value: LS_DESC}
public type LocateBookResponse record {|
    string location = "";
    boolean available = false;
|};
// Descriptor for the ListAvailableBooksResponse record type
@protobuf:Descriptor {value: LS_DESC}
public type ListAvailableBooksResponse record {|
    Book[] books = [];
|};
// Descriptor for the UpdateBookRequest record type
@protobuf:Descriptor {value: LS_DESC}
public type UpdateBookRequest record {|
    string title = "";
    string author = "";
    string location = "";
    string isbn = "";
    boolean status = false;
    string userID = "";
|};
// Descriptor for the Book record type
@protobuf:Descriptor {value: LS_DESC}
public type Book record {|
    string title = "";
    string author_1 = "";
    string location = "";
    string isbn = "";
    boolean status = false;
|};
// Descriptor for the BorrowBookRequest record type
@protobuf:Descriptor {value: LS_DESC}
public type BorrowBookRequest record {|
    string user_id = "";
    string isbn = "";
|};
// Descriptor for the CreateUserResponse record type
@protobuf:Descriptor {value: LS_DESC}
public type CreateUserResponse record {|
    string message = "";
|};
// Descriptor for the UpdateBookResponse record type
@protobuf:Descriptor {value: LS_DESC}
public type UpdateBookResponse record {|
    string message = "";
|};
// Descriptor for the CreateUserRequest record type
@protobuf:Descriptor {value: LS_DESC}
public type CreateUserRequest record {|
    string userID = "";
    string user_type = "";
|};

