import ballerina/io;

// Create a client for the Library Service running on http://localhost:9090
LibraryServiceClient ep = check new ("http://localhost:9090");
 
 // Main function that serves as the entry point
public function main() returns error? {
    io:println("Welcome to the Library Services System");
    io:println("----------------------------------------");
    io:println("1. Create User \n" +
                        "2. Library Services\n" +
                       "3. Exit");
    io:println("----------------------------------------");
    io:println("Choose your OPTION");
    string option = io:readln();

 // user's choice
    match option {
        "1" => {
            // Call the user() function to create a user
            check user();
            // Return to the main menu
            check main();
        }
        "2" => {
            // Call the bookActions() function for library services
            check bookActions();
            // Return to the main menu
            check main();
        }
        "3" =>{
            //Exit Program
        }
        _ =>{
            // Handle invalid input
            io:println("INVALID OPTION");
            // Return to the main menu
            check main();
        }
    }
}

// Function to create a user
function user() returns error?{
    io:println("----------------------------------------");
            io:println("Enter your UserID");
            string userID = io:readln();
            io:println("Enter your User Type Enter either Librarian \\Student");
            string userType = io:readln();

            io:println("----------------------------------------");
           // Create a request to create a user
           CreateUserRequest createUsersRequest = {userID: userID, user_type: userType};
           // Send the request to the Library Service
           CreateUserResponse createUsersResponse = check ep->CreateUsers(createUsersRequest);
           io:println(createUsersResponse);
}
// Function to perform library book actions
function bookActions() returns error? {
    io:println("----------------------------------------");
    io:println("1. Add a book \n" +
                        "2. Update a book \n" +
                        "3. Locate a book \n" +
                        "4. List all available books \n" +
                        "5. Borrow a book \n" +
                        "6. Remove a book\n" +
                       "7. Back to Main");
    io:println("----------------------------------------");
    io:println("Choose your OPTION");
    string option = io:readln();
    // user's Choice
    match option {
            // Code to add a book
        "1" => {
            io:println("----------------------------------------");
            io:println("Enter your user ID");
            string userID = io:readln();
            io:println("----------------------------------------");
            io:println("Enter book's title");
            string title = io:readln();
            io:println("Enter book's Author");
            string author = io:readln();
            io:println("Enter book's Location");
            string location = io:readln();
            io:println("Enter book's ISBN");
            string isbn = io:readln();
            io:println("----------------------------------------");

            AddBookRequest addBookRequest = {title: title, author: author, location: location, isbn: isbn, status: true,userID: userID};
            AddBookResponse addBookResponse = check ep->AddBook(addBookRequest);
            io:println(addBookResponse);
            check main();// Return to the main menu
        }
            // Code to update a book
        "2" => {
            io:println("----------------------------------------");
            io:println("Enter your user ID");
            string userID = io:readln();
            io:println("----------------------------------------");
            io:println("Enter book's title");
            string title = io:readln();
            io:println("Enter book's Author");
            string author = io:readln();
            io:println("Enter book's Location");
            string location = io:readln();
            io:println("Enter book's ISBN");
            string isbn = io:readln();
            io:println("----------------------------------------");

            UpdateBookRequest updateBookRequest = {title: title, author: author, location: location, isbn: isbn, status: true,userID: userID};
            UpdateBookResponse updateBookResponse = check ep->UpdateBook(updateBookRequest);
            io:println(updateBookResponse);
            check main();// Return to the main menu
        }
            // Code to locate a book
        "3" => {
            io:println("----------------------------------------");
            io:println("Enter book's isbn");
            string isbn = io:readln();
            io:println("----------------------------------------");

            LocateBookRequest locateBookRequest = {isbn: isbn};
            LocateBookResponse locateBookResponse = check ep->LocateBook(locateBookRequest);
            io:println(locateBookResponse);
            check main();// Return to the main menu
        }
            // Code to list available books
        "4" => {
            io:println("----------------------------------------");
            ListAvailableBooksRequest listAvailableBooksRequest = {};
            ListAvailableBooksResponse listAvailableBooksResponse = check ep->ListAvailableBooks(listAvailableBooksRequest);
            io:println(listAvailableBooksResponse);
            check main();// Return to the main menu
        }
            // Code to borrow a book
        "5" => {
            io:println("----------------------------------------");
            io:println("Enter your User ID");
            string userID = io:readln();
            io:println("Enter book's ISBN");
            string isbn = io:readln();
            io:println("----------------------------------------");
  
            BorrowBookRequest borrowBookRequest = {user_id: userID, isbn: isbn};
            BorrowBookResponse borrowBookResponse = check ep->BorrowBook(borrowBookRequest);
            io:println(borrowBookResponse);
            check main();// Return to the main menu
        }
            // Code to remove a book
        "6" => {
            io:println("----------------------------------------");
            io:println("Enter your user ID");
            string userID = io:readln();
            io:println("Enter book's isbn");
            string isbn = io:readln();
            io:println("----------------------------------------");

            RemoveBookRequest removeBookRequest = {isbn: isbn, userID: userID};
            RemoveBookResponse removeBookResponse = check ep->RemoveBook(removeBookRequest);
            io:println(removeBookResponse);
            check main();// Return to the main menu
        }
        "7" =>{
            check main();// Return to the main menu
        }
        _ => {
            // Handle invalid input
            io:println("----------------------------------------");
            io:println("INVALID OPTION");
            
            check main();// Return to the main menu
        }
    }
}