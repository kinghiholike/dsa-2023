import ballerina/grpc;


// Define an in-memory map to store books and user profiles.
map<Book> bookStore = {};
map<string> userProfile = {};

// Create a gRPC listener on port 9090
listener grpc:Listener ep = new (9090);

// Define the gRPC service "LibraryService"
@grpc:Descriptor {value: LS_DESC}
service "LibraryService" on ep {

    // Remote function to add a book to the library
    remote function AddBook(AddBookRequest value) returns AddBookResponse|error {
        Book book = {
            title: value.title,
            author_1: value.author,
            location: value.location,
            isbn: value.isbn,
            status: value.status
        };
        // Create a book object from the request
        if (checkUser(value.userID) == true) {
            bookStore[book.isbn] = book;
            AddBookResponse response = {isbn: book.isbn};
            return response;
        } else {
            return error("You are not Authorized to add books");
        }
    }
    // Remote function to update a book in the library
    remote function UpdateBook(UpdateBookRequest value) returns UpdateBookResponse|error {
        // Retrieve the existing book
        if (checkUser(value.userID) == true) {
            string isbn = value.isbn;
            // Update book properties if provided in the request
            if bookStore.hasKey(isbn) {

                Book existingBook = bookStore.get(isbn);

                if value.title != "" {
                    existingBook.title = value.title;
                }
                if value.author != "" {
                    existingBook.author_1 = value.author;
                }
                if value.location != "" {
                    existingBook.location = value.location;
                }
                if value.status != existingBook.status {
                    existingBook.status = value.status;
                }
                // Update the book in the store
                bookStore[isbn] = existingBook;
                // Create a response indicating success
                UpdateBookResponse response = {message: "Book updated successfully"};
                return response;
            } else {
                return error("Book not found");
            }
        }
        return error("You are not Authorised to Update a book");
    }
    // Remote function to remove a book from the library
    remote function RemoveBook(RemoveBookRequest value) returns RemoveBookResponse|error {
        // Check if the user is authorized to remove books
        if (checkUser(value.userID) == true) {
             string isbn = value.isbn;

        if bookStore.hasKey(isbn) {
            // Remove the book from the store
            _ = bookStore.remove(isbn);
            // Create a response with the updated list of books
            RemoveBookResponse response = {books: check getBooks()};
            return response;
            } else {
            return error("Book not found");
        }
        } else {
            return error("You are not Authorized to remove books");
        }   
    }// Remote function to list available books in the library
    remote function ListAvailableBooks(ListAvailableBooksRequest value) returns ListAvailableBooksResponse|error {
       // Initialize a response with an empty list of books
        ListAvailableBooksResponse response = {books: []};
     // Iterate through the book store and add available books to the response
        foreach Book book in bookStore {
            if (book.status) {
                response.books.push(book);
            }
        }

        return response;
    }

    // Remote function to locate a book in the library
    remote function LocateBook(LocateBookRequest value) returns LocateBookResponse|error {
        string isbn = value.isbn;
        LocateBookResponse response = {};

        if (bookStore.hasKey(isbn)) {
            // Retrieve the book and populate the response with its location and availability
            Book book = bookStore[isbn] ?: {};
            response.location = book.location;
            response.available = book.status;
        } else {
            return error("Book not found");
        }

        return response;
    }

    // Remote function to borrow a book from the library
    remote function BorrowBook(BorrowBookRequest value) returns BorrowBookResponse|error {
        string userId = value.user_id;
        string isbn = value.isbn;
        BorrowBookResponse response = {};

        if (bookStore.hasKey(isbn)) {
            Book book = <Book>bookStore[isbn];

            if (book.status) {
                // Mark the book as borrowed and update the store
                book.status = false;
                bookStore[isbn] = book;
                response.message = "Book " + isbn + " has been barrowed successfully by " + userId;
            } else {
                return error("Book is not available for borrowing");
            }
        } else {
            return error("Book not found");
        }
        return response;
    }
    // Remote function to create user profiles
    remote function CreateUsers(CreateUserRequest value) returns CreateUserResponse|error {
        // Attempt to create a user profile and handle errors
        error? createUserError = userCreation(value);

        if (createUserError != null) {
            return createUserError;
        }
        // Create a response indicating success
        CreateUserResponse response = {message: "User created successfully"};
        return response;
    }
}

// Function to retrieve all books from the store
function getBooks() returns Book[]|error {
    Book[] books = [];
    foreach Book book in bookStore {
        books.push(book);
    }
    return books;
}
// Function to create user profiles
function userCreation(CreateUserRequest value) returns error? {
    string userId = value.userID;

    if (userProfile.hasKey(userId)) {
        return error("User already exists with ID: " + userId);
    }
    userProfile[userId] = value.user_type;

    return null;
}
// Function to check if a user is authorized based on their user ID
function checkUser(string userID) returns ()|boolean {
    if (userProfile.hasKey(userID)) {
        string? userType = userProfile[userID];

        if (userType == "Librarian") {
            return true;
        }
    }

    return null;
}
